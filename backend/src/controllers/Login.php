<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;

class Login extends Controller
{
  protected object $user;

  public function __construct($param)
  {
    $this->user = new UserModel();

    parent::__construct($param);
  }

  public function postLogin()
  {
    return $this->user->login(
      $this->body['email'],
      $this->body['password']
    );

  }

  public function deleteUser()
  {
    return $this->user->delete(intval($this->params['id']));
  }

  public function getUser()
  {
    return $this->user->get(intval($this->params['id']));
  }
}

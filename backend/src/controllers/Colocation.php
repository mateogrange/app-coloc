<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\ColocationModel;

class Colocation extends Controller
{
  protected object $colocation;

  public function __construct($param)
  {
    $this->colocation = new ColocationModel();

    parent::__construct($param);
  }

  public function postColocation()
  {
    $this->colocation->add(
      $this->body['name'],
      $this->body['adresse'],
      $this->body['code']
    );

    return $this->colocation->getLast();
  }

  public function deleteColocation()
  {
    return $this->colocation->delete(intval($this->params['id']));
  }

  public function getColocation()
  {
    return $this->colocation->get(intval($this->params['id']));
  }
}
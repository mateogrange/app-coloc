<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\ExpenseModel;

class Expense extends Controller
{
  protected object $expense;

  public function __construct($param)
  {
    $this->expense = new ExpenseModel();

    parent::__construct($param);
  }

  public function postExpense()
  {
    $this->expense->add(
      $this->body['email'],
      $this->body['object'],
      $this->body['amount']
    );

    return $this->expense->getLast();
  }

  public function getExpense()
  {
    return $this->expense->get(intval($this->params['id']));
  }
}
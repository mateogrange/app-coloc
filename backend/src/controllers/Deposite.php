<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\DepositeModel;

class Deposite extends Controller
{
  protected object $deposite;

  public function __construct($param)
  {
    $this->deposite = new DepositeModel();

    parent::__construct($param);
  }

  public function postDeposite()
  {
    $this->deposite->add(
      $this->body['email'],
      $this->body['object'],
      $this->body['amount']
    );

    return $this->deposite->getLast();
  }

  public function getDeposite()
  {
    return $this->deposite->get(intval($this->params['id']));
  }
}
<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\JoinModel;

class Join extends Controller
{
  protected object $join;

  public function __construct($param)
  {
    $this->join = new JoinModel();

    parent::__construct($param);
  }

  public function postJoin()
  {
    $userId = $this->body['userId'];
    $colocationCode = $this->body['colocationCode'];

    error_log("userId: " . $userId);
    error_log("colocationCode: " . $colocationCode);

    if ($this->join->joinColocation($userId, $colocationCode)) {
        return ["status" => "success", "message" => "Successfully joined colocation"];
    } else {
        return ["status" => "error", "message" => "Invalid colocation code"];
    }
  }

  public function deleteUser() {
    return $this->join->delete(intval($this->params['id']));
  }


  public function getJoin()
  {
    return $this->join->get(intval($this->params['id']));
  }
}

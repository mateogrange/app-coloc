<?php

namespace App\Models;

use \PDO;
use stdClass;

class UserModel extends SqlConnect
{
  public function add($name, $lastname, $email, $password)
  {
    $token = bin2hex(random_bytes(20));

    $req = $this->db->prepare("
        INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `password`, `token`)
        VALUES (NULL, :name, :lastname, :email, :password, :token)
    ");

    $password_hash = password_hash($password, PASSWORD_BCRYPT);

    $req->execute([
      "name" => $name,
      "lastname" => $lastname,
      "email" => $email,
      "password" => $password_hash,
      "token" => $token
    ]);
  }

  public function delete(int $id)
  {
    $req = $this->db->prepare("DELETE FROM users WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get(int $id)
  {
    $req = $this->db->prepare("SELECT * FROM users WHERE id = :id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast()
  {
    $req = $this->db->prepare("SELECT * FROM users ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function login($email, $password)
  {
    $req = $this->db->prepare("
        SELECT * FROM `users` WHERE `email` = :email
    ");
    $req->execute([
      "email" => $email
    ]);
    $user = $req->fetch(PDO::FETCH_ASSOC);
    if (password_verify($password, $user['password'])) {
      return $user;
    } else {
      return null;
    }
  }
}

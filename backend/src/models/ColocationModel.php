<?php

namespace App\Models;

use \PDO;
use stdClass;

class ColocationModel extends SqlConnect
{
  public function add($name, $adresse, $code)
  {
    $req = $this->db->prepare("
        INSERT INTO `colocations` (`id`, `name`, `adresse`, `code`)
        VALUES (NULL, :name, :adresse, :code)
    ");
    $req->execute([
      "name" => $name,
      "adresse" => $adresse,
      "code" => $code
    ]);
  }

  public function delete(int $id)
  {
    $req = $this->db->prepare("DELETE FROM colocations WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get(int $id)
  {
    $req = $this->db->prepare("SELECT * FROM colocations WHERE id = :id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast()
  {
    $req = $this->db->prepare("SELECT * FROM colocations ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

}

<?php

namespace App\Models;

use \PDO;
use stdClass;

class JoinModel extends SqlConnect
{
  public function delete(int $id)
  {
    $req = $this->db->prepare("DELETE FROM colocation WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get(int $id)
  {
    $req = $this->db->prepare("SELECT * FROM ecolocation WHERE id = :id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast()
  {
    $req = $this->db->prepare("SELECT * FROM ecolocation ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function joinColocation($userId, $colocationCode)
  {
    $req = $this->db->prepare("SELECT id FROM colocations WHERE code = :code");
    $req->execute(["code" => $colocationCode]);
    $colocation = $req->fetch(PDO::FETCH_ASSOC);

    if ($colocation) {
      $colocationId = $colocation['id'];

      $updateReq = $this->db->prepare("UPDATE users SET id_coloc = :id_coloc WHERE id = :user_id");
      $updateReq->execute([
        "id_coloc" => $colocationId,
        "user_id" => $userId
    ]);

    //   return $updateReq->rowCount() > 0;
    }

    return false;
  }
}


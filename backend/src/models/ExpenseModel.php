<?php

namespace App\Models;

use \PDO;
use stdClass;

class ExpenseModel extends SqlConnect
{
  public function add($email, $object, $amount)
  {
    $req = $this->db->prepare("
        INSERT INTO `expense` (`id`, `email`, `object`, `amount`)
        VALUES (NULL, :email, :object, :amount)
    ");
    $req->execute([
      "email" => $email,
      "object" => $object,
      "amount" => $amount
    ]);
  }

  public function delete(int $id)
  {
    $req = $this->db->prepare("DELETE FROM expense WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get(int $id)
  {
    $req = $this->db->prepare("SELECT * FROM expense WHERE id = :id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast()
  {
    $req = $this->db->prepare("SELECT * FROM expense ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

}
<?php

require 'vendor/autoload.php';

use App\Router;
use App\Controllers\User;
use App\Controllers\Login;
use App\Controllers\Colocation;
use App\Controllers\Deposite;
use App\Controllers\Expense;
use App\Controllers\Join;

new Router([
  'user/:id' => User::class,
  'user/add' => User::class,
  'auth/login' => Login::class,
  'colocation/add' => Colocation::class,
  'deposite/add' => Deposite::class,
  'expense/add' => Expense::class,
  'join/colocation' => Join::class
]);

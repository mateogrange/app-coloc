import Router from './Router';
import Login from './controllers/Login';
import Register from './controllers/Register';
import Members from './controllers/Members';
import Budget from './controllers/Budget';
import Calendar from './controllers/Calendar';
import Colocation from './controllers/Colocation';
import Dashboard from './controllers/Dashboard';

const routes = [{
  url: '/',
  controller: Dashboard
},
{
  url: '/login',
  controller: Login
},
{
  url: '/register',
  controller: Register
},
{
  url: '/members',
  controller: Members
},
{
  url: '/budget',
  controller: Budget
},
{
  url: '/calendar',
  controller: Calendar
},
{
  url: '/colocation',
  controller: Colocation
}];

new Router(routes);

export default () => (`
  <div class="p-4 sm:ml-64">
    <div class="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700">
      <h1 class="text-3xl font-bold text-gray-900 ">Colocation Members</h1>
      <p class="text-lg text-gray-800">Here is the list of current members in your shared living space.</p>
    </div>
  </div>
  <div class="p-4 sm:ml-64">
    <div class="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700">
      <div class="grid grid-cols-1  md:grid-cols-3 gap-2 mb-4">
        <div class="flex items-center justify-center h-auto rounded">
          <div class="w-full h-auto max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 my-5 shadow-2xl">
            <div class="flex flex-col items-center pb-10 px-4 pt-4">
              <h5 class="mb-1 text-xl font-medium text-gray-900 dark:text-white">Joining a shared house</h5>
              <span class="text-sm text-gray-500 dark:text-gray-400">Please enter the colocation code.</span>
              <form class="joinColocationForm mt-4 md:mt-6" action="#" method="POST">
                <div class="flex">
                  <input type="text" name="colocationCode" id="colocationCode" class="colocationCodeInput block py-2.5 px-4 w-full bg-transparent border-2 border-gray-300 rounded-lg dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 text-gray-300" placeholder="Enter code" required>
                  <button type="submit" class="ml-2 inline-flex items-center px-4 py-2 text-sm font-medium text-center text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center">Join</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  `);


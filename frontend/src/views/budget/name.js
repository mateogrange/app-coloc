import img from '../../assets/images/benoit.jpeg';

export default () => (`
<li class="py-3 sm:py-4">
  <div class="flex items-center">
    <div class="flex-shrink-0">
      <img class="w-8 h-8 rounded-full" src="${img}" alt="Neil image">
    </div>
    <div class="flex-1 min-w-0 ms-4">
      <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
        Neil Sims
      </p>
      <p class="text-sm text-gray-500 truncate dark:text-gray-400">
        email@windster.com
      </p>
    </div>
    <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
        $320
    </div>
  </div>
</li>
`);

import cardMember from './card';

export default () => (`
<div class="p-4 sm:ml-64">
  <div class="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700">
    <h1 class="text-3xl font-bold text-gray-900 ">Colocation Members</h1>
    <p class="text-lg text-gray-800">Here is the list of current members in your shared living space.</p>
  </div>
</div>
<div class="p-4 sm:ml-64">
  <div class="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700">

    <div class="grid grid-cols-1  md:grid-cols-3 gap-2 mb-4">
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
      ${cardMember()}
    </div>
  </div>
</div>
`);

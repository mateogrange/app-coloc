import img from '../../assets/images/benoit.jpeg';

export default () => (`
<div class="flex items-center justify-center h-auto rounded ">
  <div class="w-full h-auto max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 my-5 shadow-2xl" >
    <div class="flex flex-col items-center pb-10 px-4 pt-4  ">
      <img class="w-24 h-24 mb-3 rounded-full shadow-lg" src="${img}" alt="Bonnie image"/>
      <h5 class="mb-1 text-xl font-medium text-gray-900 dark:text-white">Benoit Parmentier</h5>
      <span class="text-sm text-gray-500 dark:text-gray-400">member of the shared flat</span>
      <div class="flex mt-4 md:mt-6">
        <a href="#" class="inline-flex items-center px-4 py-2 text-sm font-medium text-center text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center">view profile</a>
        <a href="#" class="py-2 px-4 ms-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">Send a note</a>
      </div>
    </div>
  </div>
</div>
`);

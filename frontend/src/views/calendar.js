export default () => (`
<div class="p-4 sm:ml-64">
  <div class="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700">
      <h1 class="text-3xl font-bold text-gray-900">Calendar</h1>
      <p class="text-lg text-gray-800">Keep track of important events and activities with your shared calendar.</p>
  </div>
</div>
<div class="p-4 sm:ml-64">
  <div class="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700">
    <div id='calendar'></div>
  </div>
</div>
`);

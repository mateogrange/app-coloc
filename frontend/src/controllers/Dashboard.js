import axios from 'axios';
import viewNav from '../views/nav';
import BaseClass from '../classes/BaseClasse';
import viewDash from '../views/dashboard/dashboard';

class Chat extends BaseClass {
  constructor() {
    super();
    this.el = document.getElementById('app');
    this.run();
  }

  renderSkeleton(user) {
    return `
      <main>
        ${viewNav(user)}
        ${viewDash()}
      </main>
    `;
  }

  returnData() {
    const form = document.querySelector('.joinColocationForm');
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const userCookie = document.cookie.split('; ').find(row => row.startsWith('user='));
      const userData = JSON.parse(decodeURIComponent(userCookie.split('=')[1]));
      const userId =  userData.id;
      const inputCode = document.querySelector('.colocationCodeInput');
      const code = inputCode.value;
      this.axiosSendData(code, userId);
    });
  }

  axiosSendData(code, userId) {
    const apiUrl = 'http://localhost/join/colocation';

    axios.post(apiUrl, {
      userId: userId,
      colocationCode: code
    }, {
      headers: {
          'Content-Type': 'application/json'
      }
    }).then(response => {
      toastr.success('You have successfully join your colocation', 'Welcome Aboard')
    }).catch(error => {
      toastr.error('There was an error during joining. Please try again.')
    });
  }

  run() {
    const user = this.checkCookie();
    if (user) {
      this.el.innerHTML = this.renderSkeleton(user);
      this.initializeEvent();
      this.returnData();
    } else {
      this.el.innerHTML = this.renderSkeleton();
    }
  }

}

export default Chat;


import axios from 'axios';
import Cookies from 'js-cookie';
import viewLogin from '../views/login';

const Login = class {
  constructor() {
    this.el = document.getElementById('app');

    this.run();
  }

  renderSkeleton() {
    return `
      <main>
        ${viewLogin()}
      </main>
    `;
  }

  returnDataLogin() {
    const form = document.querySelector('.loginForm');
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const inputEmail = document.querySelector('.email');
      const email = inputEmail.value;
      const inputPassword = document.querySelector('.password');
      const password = inputPassword.value;
      this.axiosSendData(email, password);
    });
  }

  axiosSendData(email, password) {
    const apiUrl = 'http://localhost/auth/login';

    axios.post(apiUrl, {
      email: email,
      password: password
    }, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.data === null) {
        toastr.error('There was an error during log in. Please try again.', 'Login Failed');
      } else {
        window.location.href=("/");
        toastr.success('You have successfully logged in', 'Welcome Aboard');
        Cookies.set('user', JSON.stringify({
          id: response.data.id,
          name: response.data.name,
          lastname: response.data.lastname,
          email: response.data.email,
          token: response.data.token,
        }), { expires: 1 });
      }
    });
  }

  run() {
    this.el.innerHTML = this.renderSkeleton();
    this.returnDataLogin();
  }
};

export default Login;

import axios from 'axios';
import viewNav from '../views/nav';
import BaseClass from '../classes/BaseClasse';
import viewColocation from '../views/colocation';

class Colocation extends BaseClass {
  constructor() {
    super();
    this.el = document.getElementById('app');

    this.run();
  }

  renderSkeleton(user) {
    return `
    <main>
    ${viewNav(user)}
    ${viewColocation()}
    </main>
    `;
  }

  returnData() {
    const form = document.querySelector('.colocForm');
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const inputName = document.querySelector('.colocName');
      const name = inputName.value;
      const inputAdresse = document.querySelector('.colocAdresse');
      const adresse = inputAdresse.value;
      const inputCode = document.querySelector('.invitationCode');
      const code = inputCode.value;
      this.axiosSendData(name, adresse, code);
    });
  }

  axiosSendData(name, adresse, code) {
    const apiUrl = 'http://localhost/colocation/add';

    axios.post(apiUrl, {
      name: name,
      adresse: adresse,
      code: code
    }, {
      headers: {
          'Content-Type': 'application/json'
      }
    }).then(response => {
      toastr.success('You have successfully create your colocation', 'Welcome Aboard')
    }).catch(error => {
      toastr.error('There was an error during creation. Please try again.')
    });
  }

  run() {
    const user = this.checkCookie();
    if (user) {
      this.el.innerHTML = this.renderSkeleton(user);
      this.initializeEvent();
      this.returnData();
    }
  }
};

export default Colocation;

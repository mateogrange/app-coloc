import axios from 'axios';
import register from '../views/register';


const Register = class {
  constructor() {
    this.el = document.getElementById('app');
    this.run();
  }

  renderSkeleton() {
    return `
      <main>
        ${register()}
      </main>
    `;
  }

  returnData() {
    const form = document.getElementById('registerForm');
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const inputEmail = document.getElementById('email');
      const email = inputEmail.value;
      const inputName = document.getElementById('name');
      const name = inputName.value;
      const inputLastname = document.getElementById('lastname');
      const lastname = inputLastname.value;
      const inputPassword = document.getElementById('password');
      const password = inputPassword.value;
      const inputVerifPassword = document.getElementById('passwordVerif');
      const verifPassword = inputVerifPassword.value;
      this.axiosSendData(name, lastname, email, password);
    });
  }

  axiosSendData(name, lastname, email, password) {
    const apiUrl = 'http://localhost/user/add';

    axios.post(apiUrl, {
      name: name,
      lastname: lastname,
      email: email,
      password: password
    }, {
      headers: {
          'Content-Type': 'application/json'
      }
    }).then(response => {
      window.location.href=("/login");
      toastr.success('You have successfully registered!', 'Welcome Aboard')
    }).catch(error => {
      toastr.error('There was an error during registration. Please try again.', 'Registration Failed')
    });
  }

  run() {
    this.el.innerHTML = this.renderSkeleton();
    this.returnData();
  }
};

export default Register;

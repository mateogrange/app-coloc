import axios from 'axios';
import viewNav from '../views/nav';
import BaseClass from '../classes/BaseClasse';
import viewBudget from '../views/budget/budget';

class Budget extends BaseClass {
  constructor() {
    super();
    this.el = document.getElementById('app');

    this.run();
  }

  renderSkeleton(user) {
    return `
      <main>
        ${viewNav(user)}
        ${viewBudget()}
      </main>
    `;
  }

  logUserCookie() {
    const userCookie = document.cookie.split('; ').find(row => row.startsWith('user='));
    const userData = JSON.parse(decodeURIComponent(userCookie.split('=')[1]));
    document.querySelector('.emailExpense').value = userData.email;
    document.querySelector('.emailDepo').value = userData.email;
  }


  returnData() {
    const form = document.querySelector('.expenseForm');
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const inputEmail = document.querySelector('.emailExpense');
      const email = inputEmail.value;
      const inputObject = document.querySelector('.objectExpense');
      const object = inputObject.value;
      const inputAmount = document.querySelector('.amountExpense');
      const amount = inputAmount.value;
      this.axiosSendData(email, object, amount);
    });
  }

  axiosSendData(email, object, amount) {
    const apiUrl = 'http://localhost/expense/add';

    axios.post(apiUrl, {
      email: email,
      object: object,
      amount: amount
    }, {
      headers: {
          'Content-Type': 'application/json'
      }
    }).then(response => {
      toastr.success('You have successfully created your expense')
    }).catch(error => {
      toastr.error('There was an error during your expense creation. Please try again.')
    });
  }

  returnDataDepo() {
    const form = document.querySelector('.depoForm');
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const inputEmail = document.querySelector('.emailDepo');
      const email = inputEmail.value;
      const inputObject = document.querySelector('.objectDepo');
      const object = inputObject.value;
      const inputAmount = document.querySelector('.amountDepo');
      const amount = inputAmount.value;
      this.axiosSendDataDepo(email, object, amount);
    });
  }

  axiosSendDataDepo(email, object, amount) {
    const apiUrl = 'http://localhost/deposite/add';

    axios.post(apiUrl, {
      email: email,
      object: object,
      amount: amount
    }, {
      headers: {
          'Content-Type': 'application/json'
      }
    }).then(response => {
      toastr.success('You have successfully created your deposite')
    }).catch(error => {
      toastr.error('There was an error during depo. Please try again.')
    });
  }

  run() {
    const user = this.checkCookie();
    if (user) {
      this.el.innerHTML = this.renderSkeleton(user);
      document.addEventListener('DOMContentLoaded', () => {
        this.logUserCookie();
      });
      this.returnDataDepo();
      this.returnData();
      this.initializeEvent();
    }
  }

};

export default Budget;

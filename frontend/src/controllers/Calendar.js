import viewNav from '../views/nav';
import BaseClass from '../classes/BaseClasse';
import viewCalendar from '../views/calendar';

class Calendar extends BaseClass {
  constructor() {
    super();
    this.el = document.getElementById('app');

    this.run();
  }

  renderSkeleton(user) {
    return `
    <main>
    ${viewNav(user)}
    ${viewCalendar()}
    </main>
    `;
  }

  run() {
    const user = this.checkCookie();
    if (user) {
      this.el.innerHTML = this.renderSkeleton(user);
      this.initializeEvent();
    }
  }
};

export default Calendar;

import viewNav from '../views/nav';
import viewMember from '../views/members/member';
import BaseClass from '../classes/BaseClasse';

class Members extends BaseClass {
  constructor() {
    super();
    this.el = document.getElementById('app');
    this.run();
  }

  renderSkeleton(user) {
    return `
    <main>
      ${viewNav(user)}
      ${viewMember()}
    </main>
    `;
  }

  run() {
    const user = this.checkCookie();
    if (user) {
      this.el.innerHTML = this.renderSkeleton(user);
      this.initializeEvent();
    }
  }
}

export default Members;


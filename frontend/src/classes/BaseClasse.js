import Cookies from 'js-cookie';

class BaseClass {
  checkCookie() {
    const userCookie = Cookies.get('user');
    if (userCookie) {
      return JSON.parse(userCookie);
    } else if (window.location.pathname !== "/") {
      window.location.assign("/");
      return null;
    } else {
      return null;
    }
  }

  logout() {
    Cookies.remove('user');
    window.location.assign("/");
  }

  initializeEvent() {
    const signOutButton = document.querySelector('.signOut');
    if (signOutButton) {
      signOutButton.addEventListener('click', () => this.logout());
    }

    const generateCodeBtn = document.getElementById('generateCodeBtn');
    if (generateCodeBtn) {
      generateCodeBtn.addEventListener('click', () => {
        const code = 'C-' + Math.random().toString(36).substr(2, 8).toUpperCase();
        const invitationCodeInput = document.querySelector('.invitationCode');
        if (invitationCodeInput) {
          invitationCodeInput.value = code;
        }
      });
    }

    const colocForm = document.getElementById('colocationForm');
    if (colocForm) {
      colocForm.addEventListener('submit', (event) => {
        event.preventDefault();
        alert('Colocation créée avec succès!');
      });
    }
  }
}

export default BaseClass;

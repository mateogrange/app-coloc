# ColoCare - Gestion de Colocation

ColoCare est une application de gestion de colocation permettant de faciliter la gestion des colocataires, des tâches ménagères et des dépenses communes.

## Prérequis

Avant de commencer, assurez-vous d'avoir installé les outils suivants :

- Node.js
- npm (gestionnaire de paquets Node.js)
- PHP
- PhpMyAdmin
- Composer

## Installation et démarrage

### Frontend

Pour installer et démarrer le frontend de ColoCare :

1. **Installer les dépendances avec npm :**

   ```sh
   cd app-colo/frontend/
   npm install
   ```

2. **Démarrer l'application frontend :**

   ```sh
   npm start
   ```

## Backend (PHP avec Composer)

Pour installer et configurer le backend PHP avec Composer :

1. **Installer les dépendances du backend :**
   ```sh
    cd app-colo/backend/
    composer install
   ```

2. **Générer les autoloaders de Composer :**
   ```sh
    composer dump-autoload
   ```
3. **Générer les autoloaders de Composer :**

Dans backend/src/model/SqlConnect.php, modifiez les paramètres de connexion à la base de données selon votre configuration :
   ```php
   $this->host = 'localhost';
   $this->port = '3306';
   $this->dbname = 'ColoCare';
   $this->user = 'user';
   $this->password = 'mdp';
   ```
Remplacez 'localhost', '3306', 'ColoCare', 'user' et 'mdp' par les valeurs appropriées pour votre environnement de base de données.

## Configuration Nginx

Pour configurer Nginx afin de servir votre application ColoCare, utilisez le bloc de configuration suivant :
```
server {
    listen 80 default_server;
    listen [::]:80 default_server;
    
    index index.html index.htm index.nginx-debian.html index.php;
    server_name _;

    location / {
        index  index.html index.htm index.php;
        
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' 'http://127.0.0.1:7070';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'Content-Type, Authorization';
            add_header 'Access-Control-Max-Age' 86400;
            add_header 'Content-Length' 0;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            return 204;
        }

        add_header 'Access-Control-Allow-Origin' 'http://127.0.0.1:7070';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'Content-Type, Authorization';
        
        try_files $uri $uri/ /index.php;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php8.1-fpm.sock; # Assurez-vous que le chemin du socket correspond à votre configuration PHP
    }

    location ~ /\.ht {
        deny all;
    }
}

```